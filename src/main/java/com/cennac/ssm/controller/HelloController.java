package com.cennac.ssm.controller;

import com.cennac.ssm.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Cennac on 16/2/29.
 */
@Controller
public class HelloController {


    @Autowired
    UserMapper userMapper;

    @RequestMapping(value = "/hello")
    public String hello() {
        return "hello";
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/main")
    public String main() {
        return "main";
    }

    @RequestMapping(value = "/main2")
    public String main2() {
        return "main2";
    }

    @RequestMapping(value = "/insert")
    public String insert() {
//        User user=new User();
//        user.setLoginname("cc");
//        user.setName("Cennac");
//        user.setPassword("asdf");
//        user.setStatus((byte)0);
//        user.setCreatedate(new Date());
//        user.setLastLoginTime(new Date());
//
//        userMapper.insert(user);

        return "login";
    }

}
