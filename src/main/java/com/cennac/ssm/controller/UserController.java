package com.cennac.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

/**
 * Created by Cennac on 2016/3/5.
 * 用户Controller
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("")
    public String index(){
        return "userForm";
    }

    /**
     * 显示添加用户页面
     * @return
     */
    @RequestMapping(value = "add",method = RequestMethod.GET)
    public String addUser(){
        return "userForm";
    }


    /**
     * 保存用户
     * @return
     */
    @RequestMapping(value = "add",method = RequestMethod.POST)
    public String saveUser(){
        return "userForm";
    }


}
